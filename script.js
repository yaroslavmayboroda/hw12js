'use strict';

const keyStates = {};


function highlightKey(key) {

    const buttons = document.querySelectorAll('#keyboard button');


    buttons.forEach(function (button) {
        if (button.innerText === key) {
            button.classList.add('blue');
            keyStates[key] = true;
        } else {

            if (keyStates[button.innerText]) {
                button.classList.remove('blue');
                button.classList.add('black');
                keyStates[button.innerText] = false;
            }
        }
    });
}


document.addEventListener('keydown', function (event) {
    const pressedKey = event.key.toUpperCase();
    if (keyStates[pressedKey] === undefined || !keyStates[pressedKey]) {
        highlightKey(pressedKey);
    }
});